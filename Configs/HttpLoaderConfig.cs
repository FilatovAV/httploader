﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HttpLoader
{
    public class HttpLoaderConfig
    {
        public HttpLoaderConfig(string url)
        {
            Url = url;
            AllowAutoRedirect = true;
            AutomaticDecompress = true;
        }
        public string Url { get; private set; }
        public bool AutomaticDecompress { get; private set; }
        public bool AllowAutoRedirect { get; private set; }
    }
}
