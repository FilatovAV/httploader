﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HttpLoader
{
    public class SiteParserConfig
    {
        public string AnalisysPattern { get; set; }
        public SiteParserConfig(AnalysisDepth depth)
        {
            AnalisysPattern = depth switch
            {
                AnalysisDepth.Base => @"https?:\/\/[A-z.\/0-9\-]*",
                AnalysisDepth.OnlyHrefExt => @"href=\""(https?:\/\/[A-z.\/0-9\?\-#=&]*)([\""' ])",
                AnalysisDepth.AllExt => @"(https?:\/\/[A-z.\/0-9\?\-#=&]*)([\""' ])",
                _ => throw new ArgumentOutOfRangeException(nameof(depth), depth, null),
            };
        }
    }
}
