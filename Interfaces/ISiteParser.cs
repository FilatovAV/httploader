﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HttpLoader
{
    public interface ISiteParser
    {
        /// <summary> Получить список url адресов, с сортировкой. </summary>
        List<string> GetUrlsFromPage<TKey>(IEnumerable<string> site, Func<string, TKey> orderBy);
        /// <summary> Получить список url адресов. </summary>
        List<string> GetUrlsFromPage<TKey>(IEnumerable<string> site);
    }
}
