﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HttpLoader
{
    public static class ConsoleCommander
    {
        public static string GetUrlAddress()
        {
            Console.Write("Введите url адрес ( нажмите <Enter> для https://stackoverflow.com/): ");
            
            var url = Console.ReadLine();

            if (url == string.Empty)
            {
                return "https://stackoverflow.com/";
            } else
            {
                if (CheckUri(url))
                {
                    return url;
                } else
                {
                    Console.Write("Недопустимый url адрес.");
                    return GetUrlAddress();
                }
            }
        }
        private static bool CheckUri(string url)
        {
            Uri uriResult;
            return Uri.TryCreate(url, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }
        public static void ThrowExceptionMessage(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }

        public static void WriteList(List<string> list)
        {
            Console.WriteLine($"Общее число ссылок, найденных на странице: {list.Count}\n");
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
        }
    }
}
