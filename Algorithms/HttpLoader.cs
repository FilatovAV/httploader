﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace HttpLoader
{
    public class HttpLoader: IEnumerable<string>, IDisposable
    {
        private readonly Stream _stream;

        public HttpLoader(HttpLoaderConfig config)
        {
            if (config is null) { throw new ArgumentNullException("config is empty"); }

            var request = ConfigureRequest(config);

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                _stream = response.GetResponseStream();
            }
            catch (WebException ex)
            {
                ConsoleCommander.ThrowExceptionMessage(ex);
            }
            catch (System.Exception sex)
            {
                ConsoleCommander.ThrowExceptionMessage(sex);
            }
        }
        private HttpWebRequest ConfigureRequest(HttpLoaderConfig config)
        {
            var request = (HttpWebRequest)WebRequest.Create($"{config.Url}");

            request.AllowAutoRedirect = config.AllowAutoRedirect;
            request.Method = "GET";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1003.1 Safari/535.19";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.ContentType = "application/x-www-form-urlencoded";
            request.KeepAlive = true;

            if (config.AutomaticDecompress)
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            return request;
        }
        public void Dispose()
        {
            _stream?.Dispose();
        }
        public IEnumerator<string> GetEnumerator()
        {
            using var streamReader = new StreamReader(_stream, Encoding.UTF8);
            while (!streamReader.EndOfStream)
            {
                yield return streamReader.ReadLine();
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
