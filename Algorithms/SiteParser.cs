﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace HttpLoader
{
    public class SiteParser: ISiteParser
    {
        private readonly SiteParserConfig _config;
        public SiteParser(SiteParserConfig config)
        {
            _config = config;
        }
        public List<string> GetUrlsFromPage<TKey>(IEnumerable<string> site, Func<string, TKey> orderBy)
        {
            return GetMatches(site)
                .Select(s=>s.Value)
                .Distinct()
                .OrderBy(orderBy)
                .ToList();
        }
        public List<string> GetUrlsFromPage<TKey>(IEnumerable<string> site)
        {
            return GetMatches(site)
                .Select(s => s.Value)
                .Distinct()
                .ToList();
        }
        MatchCollection GetMatches(IEnumerable<string> site)
        {
            var siteString = string.Join("\n", site);
            
            var rg = new Regex(_config.AnalisysPattern, RegexOptions.Compiled);
            return rg.Matches(siteString);
        }
    }
}
