﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace HttpLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            var siteParser = new SiteParser(new SiteParserConfig(AnalysisDepth.OnlyHrefExt));
            var url = ConsoleCommander.GetUrlAddress();
            var httpLoader = new HttpLoader(new HttpLoaderConfig(url));
            var httpUrls = siteParser.GetUrlsFromPage(httpLoader, o=> o.Length);

            ConsoleCommander.WriteList(httpUrls);

            Console.ReadLine();
        }
    }
}
