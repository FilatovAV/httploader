﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HttpLoader
{
    public enum AnalysisDepth
    {
        /// <summary> Все ссылки без специфичных с параметрами. </summary>
        Base,
        /// <summary> Все ссылки с большей глубиной, но только href. </summary>
        OnlyHrefExt,
        /// <summary> Все ссылки с большей глубиной. </summary>
        AllExt
    }
}
